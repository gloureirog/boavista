$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/LoginFalho.feature");
formatter.feature({
  "line": 1,
  "name": "Login Falho no Site do Boa Vista",
  "description": "",
  "id": "login-falho-no-site-do-boa-vista",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "Login Falho",
  "description": "",
  "id": "login-falho-no-site-do-boa-vista;login-falho",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Hotmail"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "F01 - CT01 - Entrar no site do Boa Vista",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "F01 - CT01 - Clicar em Para Voce",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "F01 - CT01 - Clicar em Entrar ou Cadastrar",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "F01 - CT01 - Preencher Email",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "F01 - CT01 - Preencher Senha",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "F01 - CT01 - Clicar em Fazer Login",
  "keyword": "When "
});
formatter.match({
  "location": "Test.acessarBoaVista()"
});
formatter.result({
  "duration": 25021402419,
  "status": "passed"
});
formatter.match({
  "location": "Test.clicarParaVoce()"
});
formatter.result({
  "duration": 482500069,
  "status": "passed"
});
formatter.match({
  "location": "Test.clicarEntrarCadastrar()"
});
formatter.result({
  "duration": 45189971527,
  "status": "passed"
});
formatter.match({
  "location": "Test.preencherEmail()"
});
formatter.result({
  "duration": 393868992,
  "status": "passed"
});
formatter.match({
  "location": "Test.preencherSenha()"
});
formatter.result({
  "duration": 255380245,
  "status": "passed"
});
formatter.match({
  "location": "Test.clicarFazerLogin()"
});
formatter.result({
  "duration": 14647662264,
  "status": "passed"
});
});