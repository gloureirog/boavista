package cores;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class BasePageGUI {
	Screen screen = new Screen();

	/**
	 * @param elemento
	 * @param similaridade
	 */
	public void clicarBotaoEsquerdo(Pattern elemento, float similaridade) {
		try {
			screen.wait(elemento.similar(similaridade)).click();
		} catch (FindFailed e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param elemento
	 * @param similaridade
	 */
	public void duploClique(Pattern elemento, float similaridade) {
		try {
			screen.wait(elemento.similar(similaridade)).doubleClick();
		} catch (FindFailed e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param elemento
	 * @param similaridade
	 */
	public void clicarBotaoDireito(Pattern elemento, float similaridade) {
		try {
			screen.wait(elemento.similar(similaridade)).rightClick();
		} catch (FindFailed e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param elemento
	 * @param similaridade
	 * @param direction
	 * @param steps
	 */
	public void rolar(Pattern elemento, float similaridade, int direction, int steps) {
		try {
			screen.wait(elemento.similar(similaridade)).wheel(direction, steps);
		} catch (FindFailed e) {
			e.printStackTrace();
		}
	}
}
