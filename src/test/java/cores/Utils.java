package cores;

import static cores.DriverFactory.getDriver;

import java.io.File;
import java.io.IOException;

import org.codehaus.plexus.util.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import cucumber.api.Scenario;

/**
 * @author Paulo Lobo Neto
 * @see classe com métodos utilitários para a automação
 */
public class Utils {

	/**
	 * @param nome:
	 *            nome do arquivo à ser salvo.
	 */
	public void takeScreenshot(String nome) {
		TakesScreenshot print = (TakesScreenshot) getDriver();
		File arquivo = print.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(arquivo, new File("src" + File.separator + "test" + File.separator + "resources"
					+ File.separator + "screenshot"+ File.separator + nome + ".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param scenario
	 *            cenário atual.
	 * @param condicao
	 *            1 caso de sucesso; 2 caso de falha, e 3 para os dois casos.
	 */
	public void takeScreenshotScenario(Scenario scenario, int condicao) {
		if (scenario.isFailed() && condicao == 2) {
			byte[] screenshot = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png");
		} else if (scenario.isFailed() != false && condicao == 1) {
			byte[] screenshot = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png");
		} else if (condicao == 3) {
			byte[] screenshot = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png");
		}
	}
}