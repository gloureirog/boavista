package cores;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Paulo Lobo Neto
 *
 */
public class BasePageJava {

	WebDriver driver;
	Actions action;

	/**
	 * @param driver:
	 *            precisa receber o driver como parâmetro, para que possa realizar
	 *            todos os metódos do selenium.
	 */
	public BasePageJava(WebDriver driver) {
		this.driver = driver;
		this.action = new Actions(driver);
	}

	/**
	 * @param numero:
	 *            o parâmetro número se refere à aba que se deseja conectar.
	 */
	public void mudarAba(int numero) {
		driver.switchTo().window((String) driver.getWindowHandles().toArray()[numero]);
	}

	/**
	 * @param url:
	 *            abre um novo browser com a url informada.
	 */
	public void abrirUrl(String url) {
		driver.get(url);
	}

	/**
	 * @param url:
	 *            no browser atual, navega até a url informada.
	 */
	public void navegarUrl(String url) {
		driver.navigate().to(url);
	}

	/**
	 * @param title:
	 *            valida título da aba do browser.
	 */
	public void validarTituloDoBrowser(String title) {
		assertEquals(title, driver.getTitle());
	}

	/**
	 * @param url:
	 *            valida url atual.
	 */
	public void validarUrlAtual(String url) {
		assertEquals(url, driver.getCurrentUrl());
	}

	/**
	 * @param elemento
	 */
	public void moverParaOelemento(By elemento) {
		action.moveToElement(driver.findElement(elemento)).build().perform();
		// or action.moveToElement(driver.findElement(elemento)).perform();
	}

	/**
	 * @param elemento
	 */
	public void duploCliqueNoElemento(By elemento) {
		action.doubleClick(driver.findElement(elemento));
	}

	/**
	 * 
	 */
	public void duploClique() {
		action.doubleClick();
	}

	/**
	 * @param elementos
	 */
	public void procurarElemento(By elemento) {
		try {
			driver.findElement(elemento);
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param elemento
	 * @param valor
	 */
	public void escrever(By elemento, String valor) {
		try {
			driver.findElement(elemento).sendKeys(valor);
		} catch (NoSuchElementException e) {

		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param elemento
	 * @param valor
	 */
	public void limpar(By elemento, String valor) {
		try {
			driver.findElement(elemento).clear();
		} catch (NoSuchElementException e) {

		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param elemento
	 */
	public void clicar(By elemento) {
		try {
			driver.findElement(elemento).click();
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param Lista
	 *            de elementos
	 */
	public void clicarElementos(List<By> elementos) {
		for (By elemento : elementos) {
			try {
				driver.findElement(elemento).click();
			} catch (NoSuchElementException e) {
			} catch (TimeoutException e) {
			} catch (ElementNotVisibleException e) {
			}
		}
	}

	/**
	 * @param elemento
	 * @return boolean
	 */
	public boolean verificarSeRadioEstaMarcado(By elemento) {
		boolean retorno = false;
		try {
			retorno = driver.findElement(elemento).isSelected();
			return retorno;
		} catch (NoSuchElementException e) {
			return retorno;
		} catch (TimeoutException e) {
			return retorno;
		} catch (ElementNotVisibleException e) {
			return retorno;
		}
	}

	/**
	 * @param elemento
	 * @return string
	 */
	public String obterTexto(By elemento) {
		try {
			return driver.findElement(elemento).getText();
		} catch (NoSuchElementException e) {
			return null;
		} catch (TimeoutException e) {
			return null;
		} catch (ElementNotVisibleException e) {
			return null;
		}
	}

	/**
	 * @param elemento
	 * @param valor
	 * @return boolean
	 */
	public boolean selecionarComboEverificarSeElementoExiste(By elemento, String valor) {
		try {
			WebElement webElement = driver.findElement(elemento);
			Select combo = new Select(webElement);
			combo.selectByVisibleText(valor);
			if (!valor.isEmpty()) {
				return true;
			} else {
				return false;
			}
		} catch (NoSuchElementException e) {
			return false;
		} catch (TimeoutException e) {
			return false;
		} catch (ElementNotVisibleException e) {
			return false;
		}
	}

	/**
	 * @param elemento
	 * @param valor
	 */
	public void selecionarComboPorTextoVisivel(By elemento, String valor) {
		try {
			WebElement webElement = driver.findElement(elemento);
			Select combo = new Select(webElement);
			combo.selectByVisibleText(valor);
		} catch (NoSuchElementException e) {

		} catch (TimeoutException e) {

		} catch (ElementNotVisibleException e) {

		}
	}

	/**
	 * @param elemento
	 * @param valor
	 */
	public void clicarComboIndex(By elemento, int valor) {
		try {
			WebElement webElement = driver.findElement(elemento);
			Select combo = new Select(webElement);
			combo.selectByIndex(valor);
		} catch (NoSuchElementException e) {

		} catch (TimeoutException e) {

		} catch (ElementNotVisibleException e) {

		}
	}

	/**
	 * @param elemento
	 * @return String
	 */
	public String obterTextoCombo(By elemento) {
		try {
			WebElement element = driver.findElement(elemento);
			Select combo = new Select(element);
			return combo.getFirstSelectedOption().getText();
		} catch (NoSuchElementException e) {
			return null;
		} catch (TimeoutException e) {
			return null;
		} catch (ElementNotVisibleException e) {
			return null;
		}
	}

	/**
	 * @param elemento
	 * @return Lista de String
	 */
	public List<String> obterTextosCombo(By elemento) {
		List<String> listaDeTexto = new ArrayList<String>();
		try {
			WebElement element = driver.findElement(elemento);
			Select combo = new Select(element);
			int quantidade = obterQuantidadeOpcoesCombo(elemento);
			for (int i = 0; i <= quantidade; i++) {
				listaDeTexto.add(combo.getOptions().get(i).getText());
			}
			return listaDeTexto;
		} catch (NoSuchElementException e) {
			return null;
		} catch (TimeoutException e) {
			return null;
		} catch (ElementNotVisibleException e) {
			return null;
		}
	}

	/**
	 * @param elemento
	 * @return Integer
	 */
	public Integer obterQuantidadeOpcoesCombo(By elemento) {
		try {
			WebElement element = driver.findElement(elemento);
			Select combo = new Select(element);
			List<WebElement> options = combo.getOptions();
			return options.size();
		} catch (NoSuchElementException e) {
			return null;
		} catch (TimeoutException e) {
			return null;
		} catch (ElementNotVisibleException e) {
			return null;
		}
	}

	/**
	 * @param elemento
	 * @param valor
	 * @return boolean
	 */
	public boolean verificarSeExisteOpcaoNoCombo(By elemento, String valor) {
		try {
			WebElement element = driver.findElement(elemento);
			Select combo = new Select(element);
			List<WebElement> options = combo.getOptions();
			for (WebElement option : options) {
				if (option.getText().equals(valor)) {
					return true;
				}
			}
			return false;
		} catch (NoSuchElementException e) {
			return false;
		} catch (TimeoutException e) {
			return false;
		} catch (ElementNotVisibleException e) {
			return false;
		}
	}

	/**
	 * @param elemento
	 * @param valor
	 */
	public void desmarcarComboPorTextoVisivel(By elemento, String valor) {
		try {
			WebElement element = driver.findElement(elemento);
			Select combo = new Select(element);
			combo.deselectByVisibleText(valor);
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param elemento
	 * @return lista de String
	 */
	public List<String> obterListaDeValoresNoCombo(By elemento) {
		try {
			WebElement element = driver.findElement(elemento);
			Select combo = new Select(element);
			List<WebElement> allSelectedOptions = combo.getAllSelectedOptions();
			List<String> valores = new ArrayList<String>();
			for (WebElement opcao : allSelectedOptions) {
				valores.add(opcao.getText());
			}
			return valores;
		} catch (NoSuchElementException e) {
			return null;
		} catch (TimeoutException e) {
			return null;
		} catch (ElementNotVisibleException e) {
			return null;
		}
	}

	/**
	 * @param elemento
	 */
	public void entrarFrame(String elemento) {
		try {
			driver.switchTo().frame(elemento);
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param input
	 */
	public static void clearWithBackspace(WebElement input) {
		while (input.getAttribute("value").length() > 0) {
			input.sendKeys(Keys.BACK_SPACE);
		}
	}

	/**
	 * @param elemento
	 */
	public void limparCampo(By elemento) {
		try {
			driver.findElement(elemento).clear();
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param elemento
	 * @param elementoCss
	 *            nome do elemento CSS a ser obtido o valor. Ex. Display
	 * @return string
	 */
	public String pegarValorCss(By elemento, String elementoCss) {
		try {
			return driver.findElement(elemento).getCssValue(elementoCss);
		} catch (NoSuchElementException e) {
			return null;
		} catch (TimeoutException e) {
			return null;
		} catch (ElementNotVisibleException e) {
			return null;
		}
	}

	/**
	 * @param elemento
	 * @return string
	 */
	public String obterValorCampo(By elemento) {
		try {
			return driver.findElement(elemento).getAttribute("value");
		} catch (NoSuchElementException e) {
			return null;
		} catch (TimeoutException e) {
			return null;
		} catch (ElementNotVisibleException e) {
			return null;
		}
	}

	/**
	 * @param elemento
	 * @return boolean
	 */
	public boolean isCheckMarcado(By elemento) {
		try {
			return driver.findElement(elemento).isSelected();
		} catch (NoSuchElementException e) {
			return false;
		} catch (TimeoutException e) {
			return false;
		} catch (ElementNotVisibleException e) {
			return false;
		}
	}

	/**
	 * 
	 */
	public void sairFrame() {
		driver.switchTo().defaultContent();
	}

	/**
	 * @param elemento
	 */
	public void trocarJanela(String elemento) {
		try {
			driver.switchTo().window(elemento);
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param colunaBusca
	 * @param valor
	 * @param colunaBotao
	 * @param idTabela
	 * @return webElement
	 */
	public WebElement obterCelula(String colunaBusca, String valor, String colunaBotao, String idTabela) {
		try {
			// procurar coluna do registro
			WebElement tabela = driver.findElement(By.xpath("//*[@id='" + idTabela + "']"));
			int idColuna = obterIndiceColuna(colunaBusca, tabela);

			// encontrar a linha do registro
			int idLinha = obterIndiceLinha(valor, tabela, idColuna);

			// procurar coluna do botao
			int idColunaBotao = obterIndiceColuna(colunaBotao, tabela);

			// clicar no botao da celula encontrada
			WebElement celula = tabela.findElement(By.xpath(".//tr[" + idLinha + "]/td[" + idColunaBotao + "]"));
			return celula;
		} catch (NoSuchElementException e) {
			return null;
		} catch (TimeoutException e) {
			return null;
		} catch (ElementNotVisibleException e) {
			return null;
		}
	}

	/**
	 * @param colunaBusca
	 * @param valor
	 * @param colunaBotao
	 * @param idTabela
	 */
	public void clicarBotaoTabela(String colunaBusca, String valor, String colunaBotao, String idTabela) {
		try {
			WebElement celula = obterCelula(colunaBusca, valor, colunaBotao, idTabela);
			celula.findElement(By.xpath(".//input")).click();
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param valor
	 * @param tabela
	 * @param idColuna
	 * @return Integer
	 */
	public Integer obterIndiceLinha(String valor, WebElement tabela, int idColuna) {
		try {
			List<WebElement> linhas = tabela.findElements(By.xpath("./tbody/tr/td[" + idColuna + "]"));
			int idLinha = -1;
			for (int i = 0; i < linhas.size(); i++) {
				if (linhas.get(i).getText().equals(valor)) {
					idLinha = i + 1;
					break;
				}
			}
			return idLinha;
		} catch (NoSuchElementException e) {
			return null;
		} catch (TimeoutException e) {
			return null;
		} catch (ElementNotVisibleException e) {
			return null;
		}
	}

	/**
	 * @param coluna
	 * @param tabela
	 * @return Integer
	 */
	public Integer obterIndiceColuna(String coluna, WebElement tabela) {
		try {
			List<WebElement> colunas = tabela.findElements(By.xpath(".//th"));
			int idColuna = -1;
			for (int i = 0; i < colunas.size(); i++) {
				if (colunas.get(i).getText().equals(coluna)) {
					idColuna = i + 1;
					break;
				}
			}
			return idColuna;
		} catch (NoSuchElementException e) {
			return null;
		} catch (TimeoutException e) {
			return null;
		} catch (ElementNotVisibleException e) {
			return null;
		}
	}
	/*
	 * ***********************************
	 * 
	 * 
	 * 
	 * **************** Alerts ***********
	 * 
	 * 
	 * 
	 ***********************************/

	/**
	 * 
	 */
	public void alertaAceita() {
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	/**
	 * @return string
	 */
	public String alertaObterTexto() {
		Alert alert = driver.switchTo().alert();
		String valor = alert.getText();
		return valor;
	}

	/**
	 * 
	 */
	public void alertaNega() {
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
	}

	/**
	 * @param elemento
	 */
	public void alertaEscrever(String elemento) {
		Alert alert = driver.switchTo().alert();
		alert.sendKeys(elemento);
	}

	/*
	 * ***********************************
	 * 
	 * 
	 * 
	 * **************** Esperas **********
	 * 
	 * 
	 * 
	 ***********************************/

	/*
	 * @param number: valor em segundos.
	 */
	public void esperarThreadSleep(int time) {
		try {
			time = time * 30;
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param elemento.
	 * @param tempo
	 *            limite de espera.
	 */
	public void esperarSerClicavel(By elemento, int time) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, time);
			wait.until(ExpectedConditions.elementToBeClickable(elemento)).click();
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param elemento
	 * @param tempo
	 *            limite de espera.
	 */
	public void esperarSerelecionavel(By elemento, int time) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, time);
			wait.until(ExpectedConditions.elementToBeSelected(elemento));
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param url
	 * @param tempo
	 *            limite de espera.
	 */
	public void esperarNavegar(String url, int time) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, time);
			wait.until(ExpectedConditions.urlToBe(url));
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param elemento
	 * @param texto
	 * @param tempo
	 *            limite de espera.
	 */
	public void esperarSerClicavelClicarEscrever(By elemento, String valor, int time) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, time);
			wait.until(ExpectedConditions.elementToBeClickable(elemento)).clear();
			wait.until(ExpectedConditions.elementToBeClickable(elemento)).click();
			wait.until(ExpectedConditions.elementToBeClickable(elemento)).sendKeys(valor);
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param elemento
	 * @param time
	 */
	public void esperarVisibilidadeDoElemento(By elemento, int time) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, time);
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(elemento)));
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param elemento
	 * @param tempo
	 *            limite de espera
	 */
	public void esperarElemento(By elemento, int time) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, time);
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(elemento)));
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param elemento
	 * @param tempo
	 *            limite de espera
	 */
	public void esperarElementoSerClicavelLimparCampo(By elemento, int time) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, time);
			WebElement input = wait.until(ExpectedConditions.elementToBeClickable(elemento));
			while (input.getAttribute("value").length() > 0) {
				input.sendKeys(Keys.BACK_SPACE);
			}
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}
}