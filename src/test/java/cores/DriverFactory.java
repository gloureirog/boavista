package cores;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

import dataProvider.FileReaderManager;

/**
 * @author Paulo Lobo Neto
 *
 */
public class DriverFactory {

	private static WebDriver driver;
	private static String browser = System.getProperties().getProperty("browser");
	private static String headless = System.getProperties().getProperty("headless");
	private static FirefoxOptions firefoxOptions = new FirefoxOptions();
	private static ChromeOptions chromeOptions = new ChromeOptions();
	private static InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();

	public static String getBrowser() {
		return browser;
		
	}
	/**
	 * 
	 */
	private DriverFactory() {
	}

	/**
	 * @return retorna o driver
	 */
	public static WebDriver getDriver() {
		if (driver == null) {
			if (browser != null) {
				switch (browser) {
				case "firefox":
					System.setProperty("webdriver.gecko.driver",
							FileReaderManager.getInstance().getConfigReader().getPathDriverFirefox());

					if (configs(browser)) {
						driver = new FirefoxDriver(firefoxOptions);
					} else {
						driver = new FirefoxDriver();
					}
					break;
				case "chrome":
					System.setProperty("webdriver.chrome.driver",
							FileReaderManager.getInstance().getConfigReader().getPathDriverChrome());
					if (configs(browser)) {
						driver = new ChromeDriver(chromeOptions);
					} else {
						driver = new ChromeDriver();
					}
					break;
				case "explorer":
					System.setProperty("webdriver.ie.driver",
							FileReaderManager.getInstance().getConfigReader().getDriverPathExplorer());
					if (configs("EXPLORER")) {
						driver = new InternetExplorerDriver(internetExplorerOptions);
					} else {
						driver = new InternetExplorerDriver();
					}
					break;
				}
				if (FileReaderManager.getInstance().getConfigReader().getMaximize() == "true") {
					driver.manage().window().maximize();
				}
			} else {
				System.setProperty("webdriver.chrome.driver",
						FileReaderManager.getInstance().getConfigReader().getPathDriverChrome());
				if (configs("CHROME")) {
					driver = new ChromeDriver(chromeOptions);
				} else {
					driver = new ChromeDriver();
				}
				if (FileReaderManager.getInstance().getConfigReader().getMaximize() == "true") {
					driver.manage().window().maximize();
				}
			}
		}
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		return driver;
	}

	/**
	 * @param navegador
	 * @return boolean: há ou não alguma configuração
	 */
	public static boolean configs(String navegador) {
		if (FileReaderManager.getInstance().getConfigReader().getLerConfigs() != null
				&& FileReaderManager.getInstance().getConfigReader().getLerConfigs() == "true") {
			if (FileReaderManager.getInstance().getConfigReader().getProxy() != null) {
				if (navegador == "firefox") {
					firefoxOptions.setProxy(configProxy());
				} else if (navegador == "chrome") {
					chromeOptions.setProxy(configProxy());
				} else if (navegador == "explorer") {
					internetExplorerOptions.setProxy(configProxy());
				}
			}
			if (FileReaderManager.getInstance().getConfigReader().getHeadless() != null || headless != null) {
				if (navegador == "firefox") {
					firefoxOptions.addArguments("--headless");
				} else if (navegador == "chrome") {
					chromeOptions.addArguments("--headless");
				} else if (navegador == "explorer") {
					throw new RuntimeException("Explorer no support Headless");
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return proxy
	 */
	public static Proxy configProxy() {
		Proxy proxy = new Proxy();
		proxy.setHttpProxy(FileReaderManager.getInstance().getConfigReader().getProxy());
		proxy.setSocksUsername(FileReaderManager.getInstance().getConfigReader().getUsernameProxy());
		proxy.setSocksPassword(FileReaderManager.getInstance().getConfigReader().getPasswordProxy());
		return proxy;
	}

	/**
	 * Mata o processo do driver instanciado.
	 */
	public static void killDriver() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}
}
