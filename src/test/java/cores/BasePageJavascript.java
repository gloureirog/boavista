package cores;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author Paulo Lobo Neto
 *
 */
public class BasePageJavascript {

	private WebDriver driver;

	/**
	 * @param driver
	 */
	public BasePageJavascript(WebDriver driver) {
		this.driver = driver;
	}

	/**
	 * @param elemento
	 */
	public void clickElementJavascript(By elemento) {
		try {
			WebElement element = driver.findElement(elemento);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param quantidade
	 */
	public void scrollDown(int quantidade) {
		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0, " + quantidade + ")");
		} catch (NoSuchElementException e) {
		} catch (TimeoutException e) {
		} catch (ElementNotVisibleException e) {
		}
	}

	/**
	 * @param quantidade
	 * @return object
	 */
	public Object scrollUp(int quantidade) {
		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			return jse.executeScript("window.scrollBy(0, " + quantidade + ")");
		} catch (NoSuchElementException e) {
			return null;
		} catch (TimeoutException e) {
			return null;
		} catch (ElementNotVisibleException e) {
			return null;
		}
	}

	/**
	 * @param string
	 * @return string
	 */
	public String pegarValorCss(String string) {
		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			String value = (String) jse
					.executeScript("" + "if (document.getElementById('" + string + "').style.display == 'none'){   }");
			return value;
		} catch (NoSuchElementException e) {
			return null;
		} catch (TimeoutException e) {
			return null;
		} catch (ElementNotVisibleException e) {
			return null;
		}
	}

	/**
	 * @param elemento
	 */
	public void moverParaOelemento(By elemento) {
		WebElement element = driver.findElement(elemento);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].scrollIntoView(true);", element);
	}

}