package geradores;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GeraNome {

	/**
	 * @param param
	 * @return
	 */
	public List<String> gerarNome(int param) {

		String masculino[] = { "Amadeu", "Abel", "Abelardo", "Abigael", "Abílio", "Alceu", "Alfino", "Aleíldo",
				"Amarílio", "Alcélio", "Ananias", "Asbozeu", "Asmael", "Barnabé", "Gabriel", "Guimaraes", "Enzo",
				"Joao", "Jose", "Vitor", "Emanuel", "Wellington", "Edmar", "Caio", "Rui", "Isaque", "Edvan", "Derek",
				"Pietro", "Maxwel", "Carlos", "Jorge", "George", "Paulo", "Jonathan", "David", "Ernani", "Fagundes",
				"Giovani", "Hernani", "Ivanildo", "Jonas", "Kleber", "Leandro", "Mario", "Nivaldo", "Osvaldo",
				"Sandoval", "Marcelo", "Jairo", "Josivaldo", "Josivan" };
		
		String complemento[] = { "Villas Lobos", "Villas", "Silvério", "Sola's", "Cruz", "dos Santos", "Goncalves",
				"Lopes", "Santos", "Rosa", "Louise", "Verissimo", "Silva", "Barbosa", "Amado", "Macedo", "Machado",
				"Pessoa", "Bandeira", "Meireles", "Andrade", "Cunha", "Moraes", "de Castro", "Castro", "de Assis",
				"Nobrega", "Correia", "Alves", "Azevedo", "Ramos", "Lobo", "Souto", "Rodrigues" };
		
		String feminino[] = { "Patricia", "Clara", "Maria", "Roberta", "Monique", "Leticia", "Cecilia", "Julia",
				"Gabriele", "Gabriela", "Flávia", "Juiete", "Juliene", "Juliana", "Cristina", "Clarice", "Agatha",
				"Rachel", "Iraci", "Jodie", "Fatima", "Cleuza", "Nauva", "Neuza", "Jeiza", "Beatriz", "Carla", "Dauva",
				"Ernesta", "Giovana", "Hernania", "Izilda", "Janaina", "Kelly", "Lana", "Maraisa", "Noranei", "Abigail",
				"Judite", "Mariete", "Maiara", "Maraísa", "Josefa", "Jacicleide", "Pamela" };
		
		String sorteio[] = { "feminino", "masculino" };
		String nome = null;

		Random random = new Random();
		List<String> nomes = new ArrayList<String>();
		int resultSorteio = 0 + random.nextInt(2);
		for (int i = 0; i <= param; i++) {
			if (sorteio[resultSorteio] == "masculino") {

				int aleatorioMasculino1 = 0 + random.nextInt(36); // escolhe 1
																	// posicao de 0
																	// a 14
				int aleatorioMasculino2 = 0 + random.nextInt(36); // escolhe 1
																	// posicao de 0
																	// a 14
				int aleatorioComplemento = 0 + random.nextInt(26); // escolhe 1
																	// posicao de 0
																	// a 6
				nome = masculino[aleatorioMasculino1] + " " + masculino[aleatorioMasculino2] + " "
						+ complemento[aleatorioComplemento];
				// concatena os três aleatorios.
				// System.out.println(nome);
			} else {
				int aleatorioFeminino1 = 0 + random.nextInt(38); // escolhe 1 posicao
																	// de 0 a 14
				int aleatorioFeminino2 = 0 + random.nextInt(38); // escolhe 1 posicao
																	// de 0 a 14
				int aleatorioComplemento = 0 + random.nextInt(26); // escolhe 1
																	// posicao de 0
																	// a 6
				nome = feminino[aleatorioFeminino1] + " " + feminino[aleatorioFeminino2] + " "
						+ complemento[aleatorioComplemento];
				// concatena os três aleatorios.
				// System.out.println(nome);

			}
			nomes.add(nome);

		}
		return nomes;

	}

}
