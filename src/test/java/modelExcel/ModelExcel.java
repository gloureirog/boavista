package modelExcel;

import com.poiji.annotation.ExcelCell;
import com.poiji.annotation.ExcelCellName;

import lombok.ToString;

@ToString
public class ModelExcel {

	@ExcelCell(0)
	public String nome;

	@ExcelCellName("endereco")
	public String endereco;

	@ExcelCellName("cidade")
	public String cidade;
}
