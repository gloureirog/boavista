package cucumber;

import static cores.DriverFactory.getDriver;
import static cores.DriverFactory.killDriver;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import dataProvider.FileReaderManager;

/**
 * @version = 1.0
 * @author Paulo Lobo Neto GITHUB:
 * @CucumberExecuteTest = Classe principal, responsável pelo gerenciamento e
 *                      execução dos testes.
 * @RunWith = Quando uma classe é anotada com RunWith, JUnit invocará a classe
 *          referenciada para executar os testes.
 * @CucumberOptions = Responsável por configurar todas as especificações
 *                  definidas.
 * @Features = caminho dos arquivos ".features".
 * @Glue = caminho das classes de definição dos passos. Obs.: se não for
 *       especificado a pasta, ele procura em toda a estrutura.
 * @DryRun = Se "true", verifica se todos os passos definidos nas features estão
 *         implementados.
 * @Strict = Se "true", falha a execução dos testes se há passos indefinidos ou
 *         pendentes.
 * @Plugin = define os relatórios que serão gerados.
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/LoginFalho.feature", plugin = {
		"json:src/test/resources/cucumber-report/Resultado.json", "rerun:src/test/resources/falhas/rerun.txt",
		"junit:src/test/resources/junit-report/Resultado.xml", "html:src/test/resources/cucumber-report/",
		"pretty" }, glue = { "" }, monochrome = true, dryRun = false, strict = false)
public class CucumberExecuteTest {

	static ATUTestRecorder gravar;

	/**
	 * Método que inicia configurações antes dos testes iniciarem.
	 */
	@BeforeClass
	public static void initial() {
		try {
			gravar = new ATUTestRecorder(FileReaderManager.getInstance().getConfigReader().localOndeOvideoSeraGerado(),
					"Video", false);
			gravar.start();
		} catch (ATUTestRecorderException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Após a execução de todos os testes, o driver é finalizado.
	 */
	@AfterClass
	public static void finish() {
		if (getDriver() != null) {
			killDriver();
		}
		try {
			gravar.stop();
		} catch (ATUTestRecorderException e) {
			e.printStackTrace();
		}
	}
}