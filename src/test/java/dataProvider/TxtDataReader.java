package dataProvider;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author plobon
 *
 */
public class TxtDataReader {
	File file;

	/**
	 * 
	 */
	public TxtDataReader() {
		this.file = new File(FileReaderManager.getInstance().getConfigReader().getTxt());
	}

	/**
	 * @return
	 */
	public List<String> lendoTxt() {
		FileReader ler = null;
		try {
			ler = new FileReader(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		BufferedReader leitor = new BufferedReader(ler);
		String linha;
		List<String> linhas = new ArrayList<>();

		try {
			while ((linha = leitor.readLine()) != null) {
				linhas.add(linha);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return linhas;
	}

}
