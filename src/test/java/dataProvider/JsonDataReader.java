package dataProvider;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;

import modelJson.DataJson;

/**
 * @author Paulo Lobo Neto
 * @see classe leitora de arquivo JSON
 */
public class JsonDataReader {

	private final String customerFilePath = FileReaderManager.getInstance().getConfigReader().getPathDadosJson();
	private List<DataJson> dataJson;

	public JsonDataReader() {
		dataJson = getCustomerData();
	}

	/**
	 * @return
	 */
	private List<DataJson> getCustomerData() {
		Gson gson = new Gson();
		BufferedReader bufferReader = null;
		try {
			bufferReader = new BufferedReader(new FileReader(customerFilePath));
			DataJson[] dataJson = gson.fromJson(bufferReader, DataJson[].class);
			return Arrays.asList(dataJson);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Json file not found at path : " + customerFilePath);
		} finally {
			try {
				if (bufferReader != null)
					bufferReader.close();
			} catch (IOException ignore) {
			}
		}
	}

	/**
	 * @return
	 */
	public final DataJson getAll() {
		return dataJson.stream().findAny().get();
	}

	/**
	 * @param customerName
	 * @return
	 *//*
		 * public final DataJson getCustomerByName(final String customerName) { return
		 * dataJson.stream().filter(new Predicate<DataJson>() { public boolean
		 * test(DataJson objJson) { return
		 * objJson.cidade.equalsIgnoreCase(customerName); } }).findAny().get(); }
		 */
}