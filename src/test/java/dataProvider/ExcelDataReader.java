package dataProvider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import com.poiji.bind.Poiji;
import com.poiji.exception.PoijiExcelType;
import com.poiji.option.PoijiOptions;
import com.poiji.option.PoijiOptions.PoijiOptionsBuilder;

import modelExcel.ModelExcel;

/**
 * @author plobon
 *
 */
public class ExcelDataReader {
	private final String customerFilePath = FileReaderManager.getInstance().getConfigReader().getPlanilha();
	private List<ModelExcel> modelExcel;

	private InputStream stream;
	PoijiOptions options;

	/**
	 * 
	 */
	public ExcelDataReader() {
		modelExcel = getCustomerData();
	}

	public List<ModelExcel> excelReader() {
		return modelExcel;
	}

	/**
	 * @return
	 */
	private List<ModelExcel> getCustomerData() {
		if (customerFilePath.contains(".xlsx")) {
			options = PoijiOptionsBuilder.settings(0).build();
			try {
				stream = new FileInputStream(new File(customerFilePath));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			modelExcel = Poiji.fromExcel(stream, PoijiExcelType.XLSX, ModelExcel.class, options);
			for (int i = 0; i < modelExcel.size(); i++) {
				modelExcel.add(modelExcel.get(1));
			}
		} else {
			try {
				options = PoijiOptionsBuilder.settings(0).build();
				stream = new FileInputStream(new File(customerFilePath));
				modelExcel = Poiji.fromExcel(stream, PoijiExcelType.XLS, ModelExcel.class, options);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		System.out.println(modelExcel);
		return modelExcel;
	}
}
