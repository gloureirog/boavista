package dataProvider;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * @author Paulo Lobo Neto
 *
 */
public class ConfigFileReader {

	private Properties properties;
	private final String propertyFilePath = "src//test//resources//configs//Configurations.properties";

	/**
	 * 
	 */
	public ConfigFileReader() {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}
	}

	/**
	 * @return
	 */
	public String getPathDriverChrome() {
		String driverPath = properties.getProperty("driverChrome");
		if (driverPath != null)
			return driverPath;
		else
			throw new RuntimeException("driverChrome not specified in the Configuration.properties file.");
	}

	/**
	 * @return
	 */
	public String getPathDriverFirefox() {
		String driverPath = properties.getProperty("driverFirefox");
		if (driverPath != null)
			return driverPath;
		else
			throw new RuntimeException("driverFirefox not specified in the Configuration.properties file.");
	}

	/**
	 * @return
	 */
	public String getDriverPathExplorer() {
		String driverPath = properties.getProperty("driverExplorer");
		if (driverPath != null)
			return driverPath;
		else
			throw new RuntimeException("driverPathExplorer not specified in the Configuration.properties file.");
	}

	/**
	 * @return
	 */
	public String getMaximize() {
		String windowMaximize = properties.getProperty("windowMaximize");
		if (windowMaximize != null)
			return windowMaximize;
		else
			throw new RuntimeException("windowMaximize not specified in the Configuration.properties file.");
	}

	/**
	 * @return
	 */
	public String getApplicationUrl() {
		String url = properties.getProperty("url");
		if (url != null)
			return url;
		else
			throw new RuntimeException("url not specified in the Configuration.properties file.");
	}

	/**
	 * @return
	 */
	public String getConnectionBanco() {
		String connection = properties.getProperty("conexaoComBanco");
		String timezone = properties.getProperty("timezone");
		String usuario = properties.getProperty("usuarioDoBanco");
		String senha = properties.getProperty("senhaDoBanco");
		if (connection != null) {
			if (timezone != null) {
				if (timezone == "true")
					connection = connection + "?useTimezone=true&serverTimezone=UTC\",";
				if (usuario != null)
					connection = connection + usuario;
				if (senha != null)
					connection = connection + senha;
				return connection;
			} else {
				if (usuario != null)
					connection = connection + usuario;
				if (senha != null)
					connection = connection + senha;
				return connection;
			}

		} else {
			throw new RuntimeException("conexão not specified in the Configuration.properties file");
		}
	}

	/**
	 * @return
	 */
	public String localOndeOvideoSeraGerado() {
		String local = properties.getProperty("localDeOndeOVideoSeraGerado");
		if (local != null)
			return local;
		else
			throw new RuntimeException("Local Onde o Video Será gerado não foi especificado!");
	}

	/**
	 * @return
	 */
	public String getPlanilha() {
		String planilha = properties.getProperty("dadosPlanilha");
		if (planilha != null)
			return planilha;
		else
			throw new RuntimeException("planilha not specified in the Configuration.properties file.");
	}

	/**
	 * @return
	 */
	public String getTxt() {
		String planilha = properties.getProperty("pathTxt");
		if (planilha != null)
			return planilha;
		else
			throw new RuntimeException("planilha not specified in the Configuration.properties file.");
	}

	/**
	 * @return
	 */
	public String getPathDadosJson() {
		String testDataResourcePath = properties.getProperty("dadosJson");
		if (testDataResourcePath != null)
			return testDataResourcePath;
		else
			throw new RuntimeException(
					"Dados Json not specified in the Configuration.properties file for the Key:testDataResourcePath");
	}

	/**
	 * @return
	 */
	public String getProxy() {
		String proxy = properties.getProperty("proxy");
		if (proxy != null)
			return proxy;
		else
			return null;
	}

	/**
	 * @return
	 */
	public String getUsernameProxy() {
		String usernameProxy = properties.getProperty("usernameProxy");
		if (usernameProxy != null)
			return usernameProxy;
		else
			return null;
	}

	/**
	 * @return
	 */
	public String getPasswordProxy() {
		String passwordProxy = properties.getProperty("passwordProxy");
		if (passwordProxy != null)
			return passwordProxy;
		else
			return null;
	}

	/**
	 * @return
	 */
	public String getHeadless() {
		String headless = properties.getProperty("headless");
		if (headless != null)
			return headless;
		else
			return null;
	}
	
	/**
	 * @return
	 */
	public String getLerConfigs() {
		String lerConfigs = properties.getProperty("lerConfigs");
		if (lerConfigs != null)
			return lerConfigs;
		else
			return null;
	}
	
	/**
	 * @return
	 */
	public String getPathSikulliImages() {
		String sikulliImages = properties.getProperty("sikulliImages");
		if (sikulliImages != null)
			return sikulliImages;
		else
			return null;
	}
}
