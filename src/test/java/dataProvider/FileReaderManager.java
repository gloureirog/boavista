package dataProvider;

/**
 * @author Paulo Lobo Neto
 *
 */
public class FileReaderManager {

	private static FileReaderManager fileReaderManager = new FileReaderManager();
	private static ConfigFileReader configFileReader;
	private static JsonDataReader jsonDataReader;
	private static ExcelDataReader excelDataReader;

	/**
	 * 
	 */
	private FileReaderManager() {
	}

	/**
	 * @return
	 */
	public static FileReaderManager getInstance() {
		return fileReaderManager;
	}

	/**
	 * @return
	 */
	public ConfigFileReader getConfigReader() {
		return (configFileReader == null) ? new ConfigFileReader() : configFileReader;
	}

	/**
	 * @return
	 */
	public JsonDataReader getJsonReader() {
		return (jsonDataReader == null) ? new JsonDataReader() : jsonDataReader;
	}

	/**
	 * @return
	 */
	public ExcelDataReader getExcelReader() {
		return (excelDataReader == null) ? new ExcelDataReader() : excelDataReader;
	}
}
