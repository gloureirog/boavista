package tests;

import static cores.DriverFactory.getDriver;

import cores.BasePageJava;
import cores.Utils;
import cucumber.api.Scenario;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import dataProvider.FileReaderManager;
import model.ModelTest;
import modelJson.DataJson;

public class Test {

	BasePageJava bp = new BasePageJava(getDriver());
	ModelTest model = new ModelTest();
	DataJson dj = FileReaderManager.getInstance().getJsonReader().getAll();
	Utils util = new Utils();
	Scenario scene;

	@Given("^F01 - CT01 - Entrar no site do Boa Vista$")
	public void acessarBoaVista() {
		bp.abrirUrl(model.getUrl());
		util.takeScreenshot("BV1");
	}

	@When("^F01 - CT01 - Clicar em Para Voce$")
	public void clicarParaVoce() {
		bp.esperarSerClicavel(model.getBotParaVoce(), 60);
		util.takeScreenshot("BV2");
	}

	@When("^F01 - CT01 - Clicar em Entrar ou Cadastrar$")
	public void clicarEntrarCadastrar() {
		bp.mudarAba(1);
		bp.esperarSerClicavel(model.getBotEntrarCadastre(), 60);
		util.takeScreenshot("BV3");
	}

	@When("^F01 - CT01 - Preencher Email$")
	public void preencherEmail() {
		bp.esperarSerClicavelClicarEscrever(model.getCampoEmail(), dj.email, 60);
		util.takeScreenshot("BV4");
	}

	@When("^F01 - CT01 - Preencher Senha$")
	public void preencherSenha() {
		bp.esperarSerClicavelClicarEscrever(model.getCampoSenha(), dj.senha, 60);
		util.takeScreenshot("BV5");
	}

	@When("^F01 - CT01 - Clicar em Fazer Login$")
	public void clicarFazerLogin() {
		bp.esperarSerClicavel(model.getBotFazerLogin(), 60);
		util.takeScreenshot("BV6");
	}
}
