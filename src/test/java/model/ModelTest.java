package model;

import org.openqa.selenium.By;

import lombok.Getter;
import lombok.Setter;

@Getter
public class ModelTest {

	private String url = "https://www.boavistaservicos.com.br/";
	private By botParaVoce = By.xpath("//*[@id=\"menu-item-918\"]/a");
	private By botEntrarCadastre = By.xpath("//*[@id=\"header\"]/nav/div/div[2]/a");
	private By campoEmail = By.id("user_login");
	private By campoSenha = By.id("user_pass");
	private By botFazerLogin = By.id("wp-submit");
}
