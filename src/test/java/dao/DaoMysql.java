package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Model;

/**
 * @author plobon
 *
 */
public class DaoMysql {
	private Connection con = connection.getconnectionMySql();

	/**
	 * @return
	 */
	public List<Model> consultarTodosDaTabela() {
		String sql = "SELECT * FROM [Tabela]";

		List<Model> lista = new ArrayList<Model>();

		try {
			PreparedStatement preparador = con.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {
				/*
				 * Model model = new Model();
				 * 
				 * model.setNome(resultado.getString("nomeNoBancoDeDados"));
				 * model.setSenha(resultado.getString("sobrenomeNoBancoDeDados"));
				 * 
				 * lista.add(model);
				 */
			}
			preparador.close();
			System.out.println("Consulta efetuada com sucesso!");

		} catch (SQLException e) {
			System.out.println("Erro ao efetuar a consulta!");
		}
		return lista;
	}

	
	/**
	 * @return
	 */
	public List<Model> consultarEspecificoDaTabela() {
		String sql = "SELECT [campos] FROM [Tabela]";

		List<Model> lista = new ArrayList<Model>();

		try {
			PreparedStatement preparador = con.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();

			while (resultado.next()) {
				/*
				 * Model model = new Model();
				 * 
				 * model.setNome(resultado.getString("nomeNoBancoDeDados"));
				 * model.setSenha(resultado.getString("sobrenomeNoBancoDeDados"));
				 * 
				 * lista.add(model);
				 */			}
			preparador.close();
			System.out.println("Consulta efetuada com sucesso!");

		} catch (SQLException e) {
			System.out.println("Erro ao efetuar a consulta!");
		}
		return lista;
	}
}
