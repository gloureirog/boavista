package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import dataProvider.FileReaderManager;

/**
 * @author plobon
 *
 */
public class connection {

	/**
	 * @return
	 */
	public static Connection getconnectionMySql() {
		Connection con = null;
		try {
			con = DriverManager.getConnection(FileReaderManager.getInstance().getConfigReader().getConnectionBanco());
			System.out.println("Conectado com sucesso!");
		} catch (SQLException e) {
			System.out.println("Erro ao se conectar ao banco de dados." + e.getMessage());
		}
		return con;
	}
}